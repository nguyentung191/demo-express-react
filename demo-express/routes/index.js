const express = require("express");
const bodyParser = require("body-parser");
const MongoClient = require("mongodb").MongoClient;
const session = require("express-session");
var router = express.Router();
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static("public"));
app.use(
  session({ secret: "mySecret", resave: false, saveUninitialized: false })
);

// app.set('view engine','ejs');

const connectionString =
  "mongodb+srv://nthanhtung9x:tung1998@cluster0-zsy3m.mongodb.net/<dbname>?retryWrites=true&w=majority";
MongoClient.connect(connectionString, { useUnifiedTopology: true })
  .then((client) => {
    console.log("Connected to Database");
    const db = client.db("demo");
    const userCollection = db.collection("users");
    let listUser = [];
    db.collection("users")
      .find()
      .toArray()
      .then((result) => (listUser = [...result]))
      .catch((err) => console.log(err));

    router.get("/", (req, res) => {

    });

    // sign up
    router.get("/signup", (req, res) => {
      res.json(listUser);
      db.collection("users")
        .find()
        .toArray()
        .then((result) =>
            listUser = [...result]
        )
        .catch((err) => console.log(err));
    });
    router.post("/signup", (req, res) => {
      console.log(req.body);
      let email = req.body.email;
      let findUser = listUser.findIndex((item) => {
        return item.email === email;
      });
      if (findUser !== -1) {
        res.send('Tài khoản đã tồn tại');
      } else {
        userCollection
          .insertOne(req.body)
          .then((result) => {
            res.send('success')
          })
          .catch((error) => console.error(error));
      }
    });

    // sign in
    router.get("/signin", (req, res) => {
      db.collection("users")
        .find()
        .toArray()
        .then((result) =>
            listUser = [...result]
        )
        .catch((err) => console.log(err));
    });

    router.post("/signin", (req, res) => {
      db.collection("users")
        .find()
        .toArray()
        .then((result) =>
            listUser = [...result]
        )
        .catch((err) => console.log(err));
      let email = req.body.email;
      let password = req.body.password;
      let findUser = listUser.findIndex((item) => {
        return item.email === email && item.password === password;
      });

      if (findUser === -1) {
        res.send('Tài khoản hoặc mật khẩu không đúng');
      } else {
        res.send('success');
      }
    });

    // log out
    router.get("/logout", (req, res) => {
    });

    // manager
    router.get("/manager", (req, res) => {
      db.collection("users")
      .find()
      .toArray()
      .then((result) => (
        listUser = [...result],
        res.json(listUser)
      ))
      .catch((err) => console.log(err));

    });

    // delete user
    router.delete("/manager", (req, res) => {
      userCollection
        .deleteOne({
          email: req.body.email,
        })
        .then((result) => {
          if (result.deletedCount === 0) {
            return res.send("deleted");
          }
          res.send("success");
        })
        .catch((err) => console.err(err));
    });

    // update user
    router.put("/manager", (req, res) => {
      userCollection
        .findOneAndUpdate(
          {
            email: req.body.email,
          },
          {
            $set: {
              password: req.body.password,
            },
          },
          {
            upsert: false,
          }
        )
        .then((result) => {
          if (!result.value) {
            return res.send("Cập nhật thất bại");
          }
          res.send(`${Math.random()*10}`);
        })
        .catch((err) => console.error(err));
    });
  })
  .catch((error) => console.error(error));

module.exports = router;

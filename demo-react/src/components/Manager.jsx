import React,{useState,useEffect} from "react";
import { Redirect } from "react-router-dom";

export const Manager = () => {
  const [userLogin,setUserLogin] = useState(
    JSON.parse(localStorage.getItem('user')) || {
      email: "",
      password: ""
    }
  ); 
  const [users,setUsers] = useState([]);
  const [updateUserEmail,setUpdateUserEmail] = useState("");
  const [updateUserPassword,setUpdateUserPassword] = useState("");


  const [message, setMessage] = useState("");
  const [DeleteMessage, setDeleteMessage] = useState("");


  function fetchData(){
    fetch("http://localhost:9000/manager")
    .then(res => res.json())
    .then(res => {
        setUsers([...res])
    })
    .catch(err => err);
  }
  

  useEffect(() => {
    fetchData();
    setMessage("Cập nhật thành công");
  },[message,DeleteMessage]);

  const handleChange = (e) => {
    var name = e.target.name;
    var value = e.target.value;
    console.log(e.target.value);
      if(name === "emailUpdate"){
        setUpdateUserEmail(value);
      } else {
        setUpdateUserPassword(value);
      }

      // setUpdateUser({
      //   ...updateUser,
      //   [name]:value
      // });
  }


  const handleDelete = (item,e) => {
    e.preventDefault();
    fetch("http://localhost:9000/manager",{
      method:'delete',
      headers: {'Content-Type' : 'application/json'},
      body: JSON.stringify({
        email: item
      })
    })
    .then(res => setDeleteMessage(res))
    .catch(err => console.err(err))
  }

  const handleUpdate = (e) => {
    e.preventDefault();
    fetch("http://localhost:9000/manager",{
      method:'put',
      headers: {'Content-Type' : 'application/json'},
      body: JSON.stringify({
        email: updateUserEmail,
        password: updateUserPassword
      })
    })
    .then(res => res.text())
    .then(res => {
      setMessage(res)
      setUpdateUserEmail("")
      setUpdateUserPassword("")
    })
    .catch(err => console.err(err))
  }


  const checkValidation = () => {
    if(userLogin.email){
      return (
        <div className="container mt-4 text-center">
          <h1>Quản lý người dùng</h1>
          <div className="row">
            <div className="col-6">
              <div className="row">
                <div className="col-12 border p-4">
                  <h4>Cập nhật người dùng</h4>
                  <div className="form-group">
                    <label htmlFor="emailUpdate">Email:</label>
                    <select 
                      name="emailUpdate" 
                      id="emailUpdate" 
                      className="custom-select"
                      onChange={handleChange} 
                      value={updateUserEmail}>
                      <option value="">Chọn...</option>
                      {users.map((item,index) => {
                        return (
                          <option key={index} value={item.email}>{item.email}</option>
                        )
                      })}
                    </select>
                    <label htmlFor="passwordUpdate">Password:</label>
                    <input
                      type="password"
                      className="form-control"
                      name="passwordUpdate"
                      id="passwordUpdate"
                      placeholder="Nhập password"
                      onChange={handleChange}
                      value={updateUserPassword}
                    />
                    <button
                      id="update-btn"
                      className="btn btn-success text-white mt-3 d-block w-100"
                      onClick={(e) => handleUpdate(e)}
                    >
                      Cập nhật
                    </button>
                  </div>
                  <p id="updateMessage">
                      {message}
                  </p>
                </div>
              </div>
            </div>
            <div className="col-6">
              <table className="table border">
                <thead className="thead-dark">
                  <tr>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Feature</th>
                  </tr>
                </thead>
                <tbody>
                  { 
                    users.map((item,index) => {
                      return (
                        <tr key={index}>
                          <td>{item.email}</td> 
                          <td>{item.password}</td> 
                          <td>
                              <button 
                                className="btn btn-danger mr-2"
                                onClick={(e) => handleDelete(item.email,e)}
                              >Xoá</button>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      )
    } 
    return <Redirect to="/"/>
  }

  return (
    <>{checkValidation()} </>  
  );
};

import React, { useState } from "react";
import { Redirect } from "react-router-dom";

export const Signin = () => {
  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const [message, setMessage] = useState("");

  const handleChange = (e) => {
    var name = e.target.name;
    var value = e.target.value;
    setUser({
      ...user,
      [name]:value
    })
  }

  const signIn = (e) => {
    e.preventDefault();
    fetch("http://localhost:9000/signin", {
      method:'post',
      headers: {'Content-Type' : 'application/json'},
      body: JSON.stringify({
        email: user.email,
        password: user.password
      })
    })
    .then(res => res.text())
    .then(res => {
      console.log(res);
      if(res === 'success'){
        localStorage.setItem('user', JSON.stringify(user));
        window.location.reload(false);
      } else {
        setMessage(res)
      }
    })
    .catch(err => console.err(err))
  }

  return (
    <div>
      <h1 className="text-center">Sign In</h1>
      <div className="container">
        <form onSubmit={signIn}>
          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">
              Email
            </label>
            <div className="col-sm-10">
              <input
                type="email"
                className="form-control"
                name="email"
                id="inputEmail3"
                onChange={handleChange}
                value={user.email}
              />
            </div>
            <span className="col-sm-2" />
            <p id="message" className="text-danger col-sm-10">
              {message}
            </p>
          </div>
          <div className="form-group row">
            <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">
              Password
            </label>
            <div className="col-sm-10">
              <input
                type="password"
                className="form-control"
                name="password"
                id="inputPassword3"
                onChange={handleChange}
                value={user.password}
              />
            </div>
          </div>
          <div className="form-group row">
            <div className="col-sm-10">
              <button type="submit" className="btn btn-primary" id="signin-btn">
                Sign In
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

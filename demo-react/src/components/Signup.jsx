import React,{useState} from "react";
import { Redirect } from "react-router-dom";

export const Signup = () => {
  const [newUser,setUser] = useState({
    email: "",
    password: ""
  });

  const [message, setMessage] = useState("");

  const handleChange = (e) => {
    var name = e.target.name;
    var value = e.target.value;
    setUser({
      ...newUser,
      [name]:value
    })
  }

  const signUp = (e) => {
    e.preventDefault();
    fetch("http://localhost:9000/signup", {
      method:'post',
      headers: {'Content-Type' : 'application/json'},
      body: JSON.stringify({
        email: newUser.email,
        password: newUser.password
      })
    })
    .then(res => res.text())
    .then(res => setMessage(res))
    .catch(err => console.err(err))
  }
  
  function redirectTo() {
    return <Redirect to='/signin' />
  }

  if(message === 'success') return redirectTo();

  return (
    <div>
      <h1 className="text-center">Sign Up</h1>
      <div className="container">
        <form onSubmit={signUp}>
          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">
              Email
            </label>
            <div className="col-sm-10">
              <input
                type="email"
                className="form-control"
                name="email"
                id="inputEmail3"
                onChange={handleChange}
                value={newUser.email}
              />
            </div>
            <span className="col-sm-2" />
            <p id="message" className="text-danger col-sm-10">
              {message}
            </p>
          </div>
          <div className="form-group row">
            <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">
              Password
            </label>
            <div className="col-sm-10">
              <input
                type="password"
                className="form-control"
                name="password"
                id="inputPassword3"
                onChange={handleChange}
                value={newUser.password}
              />
            </div>
          </div>
          <div className="form-group row">
            <div className="col-sm-10">
              <button className="btn btn-primary" id="signup-btn" type="submit">
                Sign Up
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

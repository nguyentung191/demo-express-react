import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export const Header = (props) => {
  const [user,setUser] = useState(
    JSON.parse(localStorage.getItem('user')) || {
      email:"",
      password:""
    }
  );

  useEffect(() => {
    setUser(JSON.parse(localStorage.getItem('user')) || {
      email:"",
      password:""
    })
  },[user.email]);

  const logOut = () => {
    setUser({
      email:"",
      password:""
    });
    localStorage.removeItem('user');
    window.location.reload(false);
  }

  const IsCheck = () => {
    if(user.email){
      return (
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item active">
                <Link className="nav-link" to="/">
                  Home <span className="sr-only">(current)</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/manager">
                  User Manager
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/manager">
                  {user.email}
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/logout" onClick={logOut}>
                  Logout
                </Link>
              </li>
            </ul>
      )
    }
    return (
          <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
            <li className="nav-item active">
              <Link className="nav-link" to="/">
                Home <span className="sr-only">(current)</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/signin">
                Sign In
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/signup">
                Sign Up
              </Link>
            </li>
          </ul>
    )
  }

  return (
      <div>
        <nav className="navbar navbar-expand-sm navbar-light bg-light">
          <Link className="navbar-brand" to="/" >
            Demo
          </Link>
          <button
            className="navbar-toggler d-lg-none"
            type="button"
            data-toggle="collapse"
            data-target="#collapsibleNavId"
            aria-controls="collapsibleNavId"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="collapsibleNavId">
            {IsCheck()}
          </div>
        </nav>
      </div>
  );
};

import React,{useState,useEffect} from 'react';
import { BrowserRouter as Router, Switch, Route,Redirect } from "react-router-dom";
import {Header} from './components/Header';
import {Signup} from './components/Signup';
import { Signin } from './components/Signin';
import { Manager } from './components/Manager';
import { Home } from './components/Home';

function App() {

  const [user,setUser] = useState(
    JSON.parse(localStorage.getItem('user')) || {
      email:"",
      password:""
    }
  );


  // useEffect(() => {
  //   setUser(JSON.parse(localStorage.getItem('user')) || {
  //     email:"",
  //     password:""
  //   })
  // },[]);

  return (
    <Router>
      <div>
          <Header/>
          <Switch>
            <Route exact path="/">
              <Home/>
            </Route>
            <Route path="/signin">
              {user.email ? <Redirect to="/manager"/> : <Signin />}
            </Route>
            <Route path="/signup">
              <Signup/>
            </Route>
            <Route path="/logout">
              <Redirect to="/"/>;
            </Route>
            <Route path="/manager">
                <Manager/>
            </Route>
          </Switch>
      </div>
    </Router>
  )
}

export default App;
